################################################################################
#
# akku-installer
#
################################################################################

AKKU_INSTALLER_SITE =
AKKU_INSTALLER_SITE_METHOD =

AKKU_INSTALLER_DEPENDENCIES = $(call qstrip,$(BR2_PACKAGE_AKKU_INSTALLER_DEPENDENCIES))

AKKU_INSTALLER_MANIFEST = $(call qstrip,$(BR2_PACKAGE_AKKU_INSTALLER_MANIFEST_PATH))
AKKU_INSTALLER_LOCKFILE = $(call qstrip,$(BR2_PACKAGE_AKKU_INSTALLER_LOCKFILE_PATH))

AKKU_INSTALLER_PREFIX = /usr/lib/akku-packages
AKKU_INSTALLER_TARGET_DIR = $(TARGET_DIR)$(AKKU_INSTALLER_PREFIX)

AKKU_INSTALLER_TARGET_ENV = CC=$(TARGET_CC)

define AKKU_INSTALLER_BUILD_CMDS
    mkdir -p $(HOST_DIR)/usr/lib/akku/bin
    echo "#!/bin/sh" > $(HOST_DIR)/lib/akku/bin/cc
    echo '$${CC} $${CFLAGS} $${LDFLAGS} $$@' >> $(HOST_DIR)/lib/akku/bin/cc
    chmod +x $(HOST_DIR)/lib/akku/bin/cc
    test -f "$(AKKU_INSTALLER_MANIFEST)" && cp $(AKKU_INSTALLER_MANIFEST) $(@D) || :
    test -f "$(AKKU_INSTALLER_LOCKFILE)" && cp $(AKKU_INSTALLER_LOCKFILE) $(@D) || :
    ( cd $(@D) && yes | $(AKKU_INSTALLER_TARGET_ENV) PATH=$(HOST_DIR)/lib/akku/bin:$(HOST_DIR)/bin:$$PATH akku install )
endef

define AKKU_INSTALLER_INSTALL_TARGET_CMDS
    mkdir -p $(AKKU_INSTALLER_TARGET_DIR)
    rsync -ar --exclude="*.o" $(@D)/.akku $(AKKU_INSTALLER_TARGET_DIR)
    test -f "$(AKKU_INSTALLER_LOCKFILE)" && cp $(AKKU_INSTALLER_LOCKFILE) $(AKKU_INSTALLER_TARGET_DIR) || :
    echo "$(AKKU_INSTALLER_PREFIX)/.akku/env" > $(TARGET_DIR)/etc/profile.d/akku.sh
endef

$(eval $(generic-package))
