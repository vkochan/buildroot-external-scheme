################################################################################
#
# zuo
#
################################################################################

ZUO_VERSION = 1.9
ZUO_SITE = $(call github,racket,zuo,v$(ZUO_VERSION))
ZUO_LICENSE = Apache-2.0
ZUO_LICENSE_FILE = LICENSE.txt

$(eval $(autotools-package))
$(eval $(host-autotools-package))
