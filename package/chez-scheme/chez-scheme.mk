################################################################################
#
# chez-scheme
#
################################################################################

CHEZ_SCHEME_VERSION = 10.0.0
CHEZ_SCHEME_SITE = https://github.com/cisco/ChezScheme/releases/download/v$(CHEZ_SCHEME_VERSION)
CHEZ_SCHEME_SOURCE = csv$(CHEZ_SCHEME_VERSION).tar.gz
CHEZ_SCHEME_LICENSE = Apache-2.0
CHEZ_SCHEME_LICENSE_FILE = LICENSE

CHEZ_SCHEME_DEPENDENCIES = util-linux host-chez-scheme
HOST_CHEZ_SCHEME_DEPENDENCIES = host-util-linux

CHEZ_SCHEME_CONF_ENV += SCHEME=$(HOST_DIR)/bin/scheme

HOST_CHEZ_SCHEME_CONF_OPTS += --disable-curses --disable-x11
CHEZ_SCHEME_CONF_OPTS += CC_FOR_BUILD=gcc \
			 --temproot=$(TARGET_DIR) \
                         --disable-x11 \
                         --force

ifeq ($(BR2_PACKAGE_CHEZ_SCHEME_CURSES),y)
CHEZ_SCHEME_DEPENDENCIES += ncurses
else
CHEZ_SCHEME_CONF_OPTS += --disable-curses
endif

ifeq ($(BR2_PACKAGE_CHEZ_SCHEME_THREADS),y)
CHEZ_SCHEME_CONF_OPTS += --threads
CHEZ_SCHEME_THREADS = t
endif

ifeq ($(BR2_aarch64),y)
CHEZ_SCHEME_MACHINE_TYPE = arm64le
else ifeq ($(BR2_arm),y)
CHEZ_SCHEME_MACHINE_TYPE = arm32le
else ifeq ($(BR2_i386),y)
CHEZ_SCHEME_MACHINE_TYPE = i3le
else ifeq ($(BR2_x86_64),y)
CHEZ_SCHEME_MACHINE_TYPE = a6le
else ifeq ($(BR2_RISCV_64),y)
CHEZ_SCHEME_MACHINE_TYPE = rv64le
endif

CHEZ_SCHEME_FULL_MACHINE_TYPE = $(CHEZ_SCHEME_THREADS)$(CHEZ_SCHEME_MACHINE_TYPE)

CHEZ_SCHEME_CONF_OPTS += -m=$(CHEZ_SCHEME_FULL_MACHINE_TYPE)

define CHEZ_SCHEME_BUILD_CMDS
	$(TARGET_MAKE_ENV) SCHEME=$(HOST_DIR)/bin/scheme $(MAKE) -C $(@D) cross.boot
	$(TARGET_MAKE_ENV) SCHEME=$(HOST_DIR)/bin/scheme $(MAKE) -C $(@D) kernel
endef

$(eval $(autotools-package))
$(eval $(host-autotools-package))
